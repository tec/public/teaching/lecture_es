#!/bin/sh

# Shell script to setup the Embedded Systems labs
# Author: abiri
# Date: 2020-08-17
DOWNLOAD_DIR="$HOME/Embedded_Systems"
INSTALL_DIR="$HOME/ti/ccs1010"
CCS_VERSION="10.1.0.00010"

# Make sure this script is executable and readable ("chmod 755 setup_ccs.sh")
echo "Embedded Systems Installation Script - Starting..."

# Check that this script is not run as root, as it messes with $HOME
if [ $(id -u) = 0 ]
    then echo "Please do not run this script as root!"
    exit
fi

# Exit on error
set -o errexit

# Create new directory
mkdir -p $DOWNLOAD_DIR

# Setup necessary libraries
echo "Installing libraries..."
if [ $(lsb_release -rs) = "20.04" ]; then # 20.04 Focus Fossa
    echo "Installing libraries for 20.04 Focal Fossa"
    sudo apt -y -qq install wget tar unzip libc6:i386 libusb-0.1-4 libgconf-2-4 libncurses5 libpython2.7 libtinfo5
elif [ $(lsb_release -rs) = "19.10" ]; then # 19.10 Eoan Ermine
    echo "Installing libraries for 19.10 Eoan Ermine"
    sudo apt -y -qq install libc6:i386 libusb-0.1-4 libgconf-2-4 libgtk2.0-0 libncurses5 libpython2.7 libtinfo5 build-essential
elif [ $(lsb_release -rs) = "18.04" ]; then # 18.04 Bionic Beaver
    echo "Installing libraries for 18.04 Bionic Beaver"
    sudo apt -y -qq install libc6:i386 libusb-0.1-4 libgconf-2-4 build-essential
else
    echo "Unknown version... please manually install necessary packets: https://software-dl.ti.com/ccs/esd/documents/ccsv10_linux_host_support.html"
fi

# Install CCS
echo "Downloading Code Composer Studio..."
wget -q --show-progress -O "${DOWNLOAD_DIR}/ccs_installer.tar.gz" http://software-dl.ti.com/ccs/esd/CCSv10/CCS_10_1_0/exports/CCS${CCS_VERSION}_linux-x64.tar.gz
tar -xzf "${DOWNLOAD_DIR}/ccs_installer.tar.gz" -C $DOWNLOAD_DIR

echo "Installing Code Composer Studio..."
${DOWNLOAD_DIR}/CCS${CCS_VERSION}_linux-x64/ccs_setup_${CCS_VERSION}.run
echo "Installed Code Composer Studio to ${INSTALL_DIR}"

# Install CCS drivers
echo "Installing drivers..."
sudo ${INSTALL_DIR}/ccs/install_scripts/install_drivers.sh

# Download ES labs
echo "Installing lab files..."
for LAB_NR in 0 1 2 3 4
do
    LAB_DIR="${DOWNLOAD_DIR}/lab${LAB_NR}"
    wget -q -O "${LAB_DIR}.zip" "https://lectures.tik.ee.ethz.ch/es/labs/lab${LAB_NR}.zip"
    unzip -q "${LAB_DIR}.zip" -d $DOWNLOAD_DIR
    echo "Downloaded lab ${LAB_NR} to ${LAB_DIR}"
done

echo "Successfully finished installation... Happy coding!"
