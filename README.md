# Embedded Systems Lab Remote Setup Instructions

This file describes the process of setting up your own work environment for the ["Embedded Systems Lab"](https://tec.ee.ethz.ch/education/lectures/embedded-systems.html#exercise_lab) of the [Computer Engineering Group](https://tec.ee.ethz.ch/) at ETH Zurich, headed by Prof. Lothar Thiele.

We provide you with two options to run the necessary tools for the lab:
- [Virtual machine installation](#virtual-machine-installation) (**recommended**) uses a virtual machine that has already been set-up by us. We recommend this option if you are less experienced with setting up your own tool chains. For this, you will have to install a virtual machine manager called *VirtualBox* and can then directly run the required software (*Code Composer Studio*) inside the virtual machine.
- [Native installation](#native-installation) (**without support**) uses your normal operating system and directly installs the necessary libraries and tools through an installer script. Please notice that this might change the configurations of your machine. We currently only provide scripts for Ubuntu 18.04, Ubuntu 19.10 and Ubuntu 20.04. While we hope that these work well for you, we do not officially support such installations; if you run into problems, please try using a [VM](#virtual-machine-installation) before asking us for help. However, this method might be suitable to you if you cannot get the VM to run.

## Virtual machine installation

A *virtual machine* (VM) emulates a complete computer system on top of your normal operating system. In such a case, you run a *guest* system (which we previously prepared for you) on top of your own *host* system (just as you run any other programs). For the ES lab, we use Linux [Ubuntu 20.04.1 LTS](https://releases.ubuntu.com/20.04.1/) as a guest system.

The VM is available [from our servers](https://pub.tik.ee.ethz.ch/lehre/es/remote/es_labs.ova) and requires 12 GB of free disk space as well as at least 2 GB of RAM.

To run the VM, we use [VirtualBox 6.1.12](https://www.virtualbox.org/). This program is a so-called *virtual machine manager* that provides the link between all common host systems (Windows, macOS, Linux) and the guest system (in our case Linux Ubuntu).

### Installing Virtualbox

The installation process of VirtualBox depends on your host operating system. For further information, we refer to the [official installation guides](https://www.virtualbox.org/wiki/Downloads). Please note that it could be that you have to enable certain hardware virtualization options in your BIOS to enable virtual machines; VirtualBox should in most cases prompt if there is an issue and tell you what to do. However, if you cannot get a VM to work, please check out whether you have settings such as `Intel Virtualization Technology` for an Intel processor or `AMD-V` for an AMD one (or see [further instructions](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-troubleshooting-enabling_intel_vt_x_and_amd_v_virtualization_hardware_extensions_in_bios)).

#### Windows

Simply download the VirtualBox `6.1.12` installer from the [official servers](https://download.virtualbox.org/virtualbox/6.1.12/VirtualBox-6.1.12-139181-Win.exe). 
After the download finished, click on the executable to install the program. 
During the installation, make sure that the option `VirtualBox USB Support` is enabled. 

#### macOS

Simply download the VirtualBox `6.1.12` installer from the [official servers](https://download.virtualbox.org/virtualbox/6.1.12/VirtualBox-6.1.12-139181-OSX.dmg). 
After the download finished, click on the file to install the program.

In case you are using macOS *High Sierra* (10.13), you might need to [adjust your security settings](https://apple.stackexchange.com/a/301305) to successfully install VirtualBox.

In case you are using macOS *Catalina* (10.15) and experience crashes of the VirtualBox, [follow these instructions](https://unix.stackexchange.com/questions/609077/how-to-fix-virtualbox-vm-quit-unexpectedly-when-running-my-ubuntu-20-04-lts-vm) and uncheck `Enable Audio` in the VirtualBox settings. Another reason for a crash could be [privacy violations](https://developer.apple.com/forums/thread/659732), so you might have to run VirtualBox as the superuser (`sudo virtualbox`).

#### Linux

The installation of VirtualBox depends on your Linux flavour. You can find an overview over various distributions [online](https://www.virtualbox.org/wiki/Linux_Downloads).

For **Ubuntu 19.10 / 20.04** and **Ubuntu 18.04**, you can install the software packages directly by downloading the [files for Ubuntu 19.10 / 20.04](https://download.virtualbox.org/virtualbox/6.1.12/virtualbox-6.1_6.1.12-139181~Ubuntu~eoan_amd64.deb) or the [files for Ubuntu 18.04](https://download.virtualbox.org/virtualbox/6.1.12/virtualbox-6.1_6.1.12-139181~Ubuntu~bionic_amd64.deb). While you can also install VirtualBox through the package manager for Debian-based distributions, we discourage you from doing so as you might accidentally acquire a newer version than the one we support (`6.1.12`).

Please make sure that you follow the [guidance on USB devices on Linux](#linux-establishing-usb-connections) before trying to start CCS.

**UEFI Secure Boot:** In case you have enabled *Secure Boot*, the installation script will automatically detect this and prompt that you must re-start your computer for third-party drivers to be installed.
First, you must define a password (we recommend a simple password without special symbols) that you must then enter again after you have restarted your host OS. During booting, it will prompt you with the option to `Enroll MOK`. Choose this option and add the new key using the password you just defined before.

#### Extension Pack (optional)

In case you should run into problems using devices which are connected through USB, consider installing the [VirtualBox Extension Pack](https://download.virtualbox.org/virtualbox/6.1.12/Oracle_VM_VirtualBox_Extension_Pack-6.1.12.vbox-extpack).
The extension pack allows you to make use of USB 2.0 and 3.0 features, which increases the speed of your interactions with the LaunchPad.

Simply download the file and double-click on it; VirtualBox should then automatically install the extensions.

### Running your virtual machine

After having downloaded the VM [from our servers](https://pub.tik.ee.ethz.ch/lehre/es/remote/es_labs.ova) as an `.ova` file, you can import the machine into VirtualBox by double-clicking on the downloaded `.ova` file and VirtualBox will ask to import the VM. 
During the import, make sure that the box next to `Import hard drives as VDI` is ticked.

Before you start the VM, consider adapting the VirtualBox VM settings in the VirtualBox Manager. We recommend the following configuration:
 * Base Memory (RAM): 4096 MB
 * Processors: 2 or more
 * Video Memory: 64 MB or more

#### Windows: Hyper-V configuration

If you are using Windows, make sure that Hyper-V is disabled. This is especially important if you previously used Docker or WSL2, as they prevent VirtualBox from booting a VM; for further information, see the [WSL issue](https://github.com/MicrosoftDocs/WSL/issues/536), the [VirtualBox issue](https://forums.virtualbox.org/viewtopic.php?t=99335) and an overall discussion on [Hyper-V and VirtualBox](https://forums.virtualbox.org/viewtopic.php?t=62339).

To disable Hyper-V, go to `Control Panel` &rarr; `Programs` &rarr; `Turn Windows features on or off` and make sure that the following features are disabled:
- `Hyper-V`
- `Virtual Machine Platform`
- `Windows Hypervisor Platform`
- `Windows Subsystem for Linux`

You might have to restart your system for these changes to take effect.
After you successfully disabled Hyper-V, you should see a blue "V" as the third icon on the bottom right of the VirtualBox window once you started the VM. If there is a green turtle or if your VM remains stuck during booting, then Hyper-V is still enabled!

#### Linux: Establishing USB connections

For the USB devices to be available on Ubuntu and other Linux distributions, you must add your user (on the host OS) to the `vboxusers` group.

To do so, perform the following action in your Ubuntu host OS (replace `yourusername` with your personal username on the host OS, *not* `student`):

    sudo adduser yourusername vboxusers
    
**Log out and back in again** for the changes to take effect. You can verify that your user was successfully added to the group by running:

    groups yourusername
    
More information on connecting USB devices to VMs can be found in the [official Ubuntu documentation](https://help.ubuntu.com/community/VirtualBox/USB).

#### Ubuntu VM

Now, you are ready to start your VM by double-clicking on it in the VirtualBox Manager. The provided virtual machine is built on Linux [Ubuntu 20.04.1 LTS](https://releases.ubuntu.com/20.04.1/). As with any physical machine, you will see the normal desktop screen after booting up.
You should be logged in automatically. However, in case you are required to authenticate yourself (e.g. to run the `sudo` command), we prepared a user `student` with password `student`.

The VM should automatically scale its resolution to fit your window size; if not, make sure that `View` &rarr; `Auto-resize Guest Display` is ticked in your VM menu bar. To manually change your screen resolution, open your VM and inside the guest OS go to `Settings` &rarr; `Displays` &rarr; `Resolution`. The VM should automatically adjust its window size thereafter.

The default keyboard layout for the VM is "German (Switzerland)". If you would like to change this, follow the [official guide for alternative keyboard layouts](https://help.ubuntu.com/stable/ubuntu-help/keyboard-layouts.html.en): inside the guest OS go to `Settings` &rarr; `Regions & Languages` &rarr; `Input Sources` and use the `+` button to add the language that is associated with the desired layout.

To start the integrated development environment (IDE) for the ES labs, [Code Composer Studio](#code-composer-studio-ccs), simply either double-click on the corresponding desktop icon or left-click on the icon in the task bar. You can find the labs already pre-downloaded in `~/Embedded_Systems` (use the `Files` program to browse through them).


## Code Composer Studio (CCS)

We use Texas Instrument's `Code Composer Studio` [10.1.0](https://www.ti.com/tool/download/CCSTUDIO). The [CCS Document Overview](https://software-dl.ti.com/ccs/esd/documents/ccs_documentation-overview.html) and [CCS User's Guide](https://software-dl.ti.com/ccs/esd/documents/users_guide_10.1.0/index.html) might be helpful if you have further questions. There is a dedicated guide for running CCS on [virtual machines](https://software-dl.ti.com/ccs/esd/documents/ccs_vmware.html) in case you run into issues.

### Importing projects


**VM installation**: If you use the VM provided by us, the CCS workspace should already be set up correctly.

**Native installation**: First, add the labs to your CCS workspace; you can find them dowloaded by the installer script as `~/Embedded_Systems/labX.zip` (where `X` is the lab number). Inside CCS, use `File` &rarr; `Import` &rarr; `Code Composer Studio` &rarr; `CCS Projects` and select the unzipped folder in the `Select search-directory:` field.
Notice that you can also add the `.zip` versions through `Select archive file:`. 

### Connecting your LaunchPad

Your host OS should automatically detect the LaunchPad. Notice that on certain systems such as Windows, you might have to wait a few seconds until the necessary drivers have been installed in the background. To make it available to the virtual machine, do as follows:
 
**Step 1:** Plug-in your LaunchPad to your host computer using the provided microUSB cable. Make sure that CCS is *not* running on the host system, as it might capture the LaunchPad before the VM can do so.

**Step 2:** In the VirtualBox menu of the running virtual machine (top left), click on `Devices` &rarr; `USB` &rarr; `Texas Instruments XDS110 [...]` to make the device available to your virtual machine (there should be a tick next to the option after you have selected it). You can see a visualization of this option in the screenshot below.

In case you do not see `Texas Instruments XDS110 [...]` as an available option, make sure that you have followed our [guidance on USB devices on Linux](#linux-establishing-usb-connections) and that the device is visible on the host system. On Windows, you can verify this by going to `Control Panel` &rarr; `Hardware and Sound` &rarr; `Devices and Printers` where you should see `XDS110 [...]` listed under "Unspecified".

### Updating your LaunchPad

Before your first launch, you must update the firmware of your LaunchPad. At the time of writing, the up-to-date version of the LaunchPad is `3.0.0.13`; if your board has an older version flashed onto it, CCS will automatically detect this and propose an upgrade. If you run CCS on your native machine, the upgrade should successfully complete; on the VM, do as follows:

**Step 1:** Make sure the device is made available to the VM (`Devices` &rarr; `USB` &rarr; `Texas Instruments XDS110 [...]` is selected in the VirtualBox menu on the top left; see also screenshot below). If you see `Texas Instruments Incorporated Tiva Device Firmware Update`, directly jump to *Step 3*. You can check that the device is correctly recognized by the VM by listing all available TI devices:

    ~/ti/ccs1010/ccs/ccs_base/common/uscif/xds110/xdsdfu -e

**Step 2:** Switch the device into DFU mode by entering into a terminal:

    ~/ti/ccs1010/ccs/ccs_base/common/uscif/xds110/xdsdfu -m

**Step 3:** You should now see a new device in your list in the VirtualBox menu: `Devices` &rarr; `USB` &rarr; `Texas Instruments Incorporated Tiva Device Firmware Update`. Make sure that it is selected and enter into the same terminal as before:

    ~/ti/ccs1010/ccs/ccs_base/common/uscif/xds110/xdsdfu -f ~/ti/ccs1010/ccs/ccs_base/common/uscif/xds110/firmware_3.0.0.13.bin -r

Make sure that you do not forget the `-r` at the end of the command. You can also reset the device separately by entering `~/ti/ccs1010/ccs/ccs_base/common/uscif/xds110/xdsdfu -r`.

**Step 4:** Once again, connect the debug probe by clicking on `Devices` &rarr; `USB` &rarr; `Texas Instruments XDS110 [...]`. Notice that the name of the device should now have the new version appended (`Texas Instruments XDS110 (3.0.0.13)`). In the end, you should see the VM as depicted in the following screenshot (notice the terminal on the right where the instructions used to update the LaunchPad are visible):

<img src="https://gitlab.ethz.ch/tec/public/teaching/lecture_es/-/raw/master/screenshot_usb.png" width="50%" />


## Native installation

We provide a script to install the necessary Code Composer Studio (CCS) software on Ubuntu 18.04, 19.10 and 20.04. Please notice that it is possible to [setup CCS natively for other OSes](https://www.ti.com/tool/download/CCSTUDIO) such as Windows 10 and macOS and skip Step 1; we will however only provide support for Ubuntu VMs of the previously mentioned versions.

**Step 1:** Use our [installer script](./setup_ccs.sh) to install the necessary libraries as well as [Code Composer Studio](#code-composer-studio-ccs). Feel free to inspect and adapt the script in case you try to run it on different distributions or would like to specify the installation directory. The default download directory for the script is `~/Embedded_Systems`. If you decide against using the script, on Linux make sure to [install the necessary drivers from TI](https://software-dl.ti.com/ccs/esd/documents/users_guide_10.1.0/ccs_installation.html#driver-installation) after the installation.

**Step 2:** You can find a [step-by-step tutorial](https://software-dl.ti.com/ccs/esd/documents/users_guide_10.1.0/ccs_installation.html) of the CCS installation from TI; you should choose the default location, then `Custom Installation` and only select `SimpleLink MSP432 low power + performance MCUs` as a component. You do not require support for any additional debug probes.

**Step 3:** After the script / your manual installation has finished, open CCS through one of two ways:
1. For Ubuntu, select `Show Applications` (bottom left) and right-click on the icon in the list to `Add to Favorites`.
2. For Ubuntu 18.04, simply double-click on the desktop icon and select `Trust and Launch`. For Ubuntu 20.04, first right-click on the desktop icon and select `Allow launching`. Then double-click to launch the application.

For other operating systems, you should find a desktop shortcut and can use it as any other program.

**Step 4:** Create a new workspace.

**Step 5:** Update CCS by clicking on the pop-up that should appear automatically or clicking on the icon in the bottom right corner. You should see two updates:
- `ARM Compiler Tools`
- `ARM GCC Compiler Tools`
